package com.svalh.data.service.source

import com.svalh.data.database.AppDatabase
import com.svalh.data.mapper.toDataBaseAddMovie
import com.svalh.data.mapper.toDataBaseMovie
import com.svalh.data.mapper.toDomainMovie
import com.svalh.data.mapper.toDomainMovieCart
import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.source.DataBaseDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RoomDataSource(private val appDatabase: AppDatabase) : DataBaseDataSource {

    override suspend fun popularMovies(): List<DomainMovie> = withContext(Dispatchers.IO) {
        appDatabase.movieDao().getAll().map {
            it.toDomainMovie()
        }
    }

    override suspend fun isEmpty(): Boolean = withContext(Dispatchers.IO) {
        (appDatabase.movieDao().movieCount()) <= 0
    }

    override suspend fun saveMovies(movies: List<DomainMovie>) = withContext(Dispatchers.IO) {
        val movieList = movies.map {
            it.toDataBaseMovie()
        }

        appDatabase.movieDao().insertMovies(movieList)
    }

    override suspend fun addMovieToCart(movies: DomainMovie) {
        val addMovie = movies.run {
            this.toDataBaseAddMovie()
        }
        appDatabase.movieCartDao().insertMovieToCart(addMovie)
    }

    override suspend fun deleteMovieFromCart(id: Int) = withContext(Dispatchers.IO) {
        appDatabase.movieCartDao().deleteMovieFromCart(id)
    }

    override suspend fun getMovieFromCartById(id: Int): DomainMovie = withContext(Dispatchers.IO) {
        appDatabase.movieCartDao().getMovieFromCartById(id).run {
            this.toDomainMovie()
        }
    }

    override suspend fun getMoviesFromCart(): List<DomainMovie> = withContext(Dispatchers.IO) {
        appDatabase.movieCartDao().getAllFromCart().map {
            it.toDomainMovieCart()
        }
    }

    override suspend fun deleteAllCart() = withContext(Dispatchers.IO) {
        appDatabase.movieCartDao().deleteAll()
    }
}