package com.svalh.data.service.source

import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.svalh.data.ResponseHandler
import com.svalh.data.mapper.toDomainMovie
import com.svalh.data.mapper.toDomainMovieImages
import com.svalh.data.service.MovieDbServices
import com.svalh.data.service.response.PopularMoviesDbResult
import com.svalh.domain.common.Resource
import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.entities.DomainMovieImages
import com.svalh.domain.source.RemoteDataSource
import org.json.JSONObject


class RemoteDataSource(
    private val movieDbServices: MovieDbServices,
    private val apiKey: String,
    private val volleyRequest: RequestQueue
) : RemoteDataSource {

    companion object {
        private const val DEFAULT_LANGUAGE = "en-US"
        private const val DEFAULT_PAGE = "1"
        private const val VOLLEY_URL_REQUEST_ALL_MOVIES =
            "https://api.themoviedb.org/3/movie/popular?api_key=4409d04687217e7763238e91871d61c3&language=\"en-US\"&page=\"1\""
    }

    override suspend fun listPopularMovies(): Resource<List<DomainMovie>> {
        return try {

            val volleyMovies: PopularMoviesDbResult? = null

            val jsonObjectRequest =
                JsonObjectRequest(Request.Method.GET, VOLLEY_URL_REQUEST_ALL_MOVIES, null,
                    Response.Listener { response ->

                        for (i in 0 until response.length()){
                            volleyMovies?.results?.map {
                                it.backdropPath = response.getString("backdrop_path")
                            }
                        }
                    },
                    Response.ErrorListener { error ->
                        print(error.toString())
                    }
                )


            volleyRequest.add(jsonObjectRequest)


            val movies = movieDbServices.popularMovies(
                apiKey = apiKey,
                language = DEFAULT_LANGUAGE,
                page = DEFAULT_PAGE
            ).run {
                results.map {
                    it.toDomainMovie()
                }
            }

            ResponseHandler().handleSuccess(movies)
        } catch (e: Exception) {
            ResponseHandler().handleException(e)
        }
    }

    override suspend fun movieDetail(movieId: Int): Resource<DomainMovie> {
        return try {
            val movie = movieDbServices.movieDetail(
                id = movieId,
                apiKey = apiKey,
                language = DEFAULT_LANGUAGE
            ).run {
                this.toDomainMovie()
            }

            ResponseHandler().handleSuccess(movie)
        } catch (e: Exception) {
            ResponseHandler().handleException(e)
        }
    }

    override suspend fun getMovieImage(id: Int): Resource<DomainMovieImages> {
        return try {
            val movieImage = movieDbServices.getMovieImages(
                id = id,
                apiKey = apiKey
            )
            ResponseHandler().handleSuccess(movieImage.toDomainMovieImages())
        } catch (e: Exception) {
            ResponseHandler().handleException(e)
        }
    }
}