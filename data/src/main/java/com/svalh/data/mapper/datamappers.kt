package com.svalh.data.mapper

import com.svalh.data.database.entity.DataBaseAddGenre
import com.svalh.data.database.entity.DataBaseAddMovie
import com.svalh.data.database.entity.DataBaseGenre
import com.svalh.data.database.entity.DataBaseMovie
import com.svalh.data.service.response.Movie
import com.svalh.data.service.response.MovieDbResult
import com.svalh.data.service.response.MovieImages
import com.svalh.domain.entities.*

fun MovieDbResult.toDomainMovie(): DomainMovie {
    val listGenres = ArrayList<DomainGenre>()

    this.genres.forEach { (id, name) ->
        listGenres.add(DomainGenre(id, name))
    }

    return DomainMovie(
        backDropPath,
        listGenres,
        id,
        originalTitle,
        overview,
        posterPath,
        releaseData,
        title,
        video,
        voteAverage
    )
}

fun Movie.toDomainMovie(): DomainMovie {
    val listGenres = ArrayList<DomainGenre>()
    genreIds.map {
        listGenres.add(DomainGenre(it, ""))
    }

    return DomainMovie(
        backdropPath,
        listGenres,
        id,
        originalTitle,
        overview,
        posterPath,
        releaseDate,
        title,
        video,
        voteAverage
    )
}

fun DomainMovie.toDataBaseMovie(): DataBaseMovie {
    val listGenres = ArrayList<DataBaseGenre>()
    domainGenres.map {
        listGenres.add(DataBaseGenre(it.id, it.name))
    }

    return DataBaseMovie(
        id,
        title,
        backdropPath,
        listGenres,
        originalTitle,
        overview,
        posterPath,
        releaseDate,
        video,
        voteAverage
    )

}

fun DomainMovie.toDataBaseAddMovie(): DataBaseAddMovie {
    val listGenres = ArrayList<DataBaseAddGenre>()
    domainGenres.map {
        listGenres.add(DataBaseAddGenre(it.id, it.name))
    }

    return DataBaseAddMovie(
        id,
        title,
        backdropPath,
        listGenres,
        originalTitle,
        overview,
        posterPath,
        releaseDate,
        video,
        voteAverage
    )

}

fun DataBaseGenre.toDomainGenre() = DomainGenre(
    id,
    name
)

fun DataBaseMovie.toDomainMovie(): DomainMovie {
    val listGenres = ArrayList<DomainGenre>()
    dataBaseGenres?.map {
        listGenres.add(DomainGenre(it.id, it.name))
    }

    return DomainMovie(
        backdropPath,
        listGenres,
        id,
        originalTitle,
        overview,
        posterPath,
        releaseDate,
        title,
        video,
        voteAverage
    )
}

fun DataBaseAddMovie.toDomainMovieCart(): DomainMovie {
    val listGenres = ArrayList<DomainGenre>()
    dataBaseGenres?.map {
        listGenres.add(DomainGenre(it.id, it.name))
    }

    return DomainMovie(
        backdropPath,
        listGenres,
        id,
        originalTitle,
        overview,
        posterPath,
        releaseDate,
        title,
        video,
        voteAverage
    )
}

fun DataBaseAddMovie.toDomainMovie(): DomainMovie {
    val listGenres = ArrayList<DomainGenre>()
    dataBaseGenres?.map {
        listGenres.add(DomainGenre(it.id, it.name))
    }

    return DomainMovie(
        backdropPath,
        listGenres,
        id,
        originalTitle,
        overview,
        posterPath,
        releaseDate,
        title,
        video,
        voteAverage
    )
}

fun MovieImages.toDomainMovieImages(): DomainMovieImages {
    val backdrops = ArrayList<DomainBackdrops>()
    val posters = ArrayList<DomainPosters>()

    this.backdrops.forEach { backdrop ->
        with(backdrop) {
            backdrops.add(
                DomainBackdrops(
                    aspectRatio,
                    filePath,
                    height,
                    iso_639_1,
                    voteAverage,
                    voteCount,
                    width
                )
            )
        }
    }

    this.posters.forEach { poster ->
        with(poster) {
            posters.add(
                DomainPosters(
                    aspectRatio,
                    filePath,
                    height,
                    iso_639_1,
                    voteAverage,
                    voteCount,
                    width
                )
            )
        }
    }

    return DomainMovieImages(
        id,
        backdrops,
        posters
    )
}