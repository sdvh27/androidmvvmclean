package com.svalh.data.repositories.db

import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.repositories.db.DBAddMovieRepository
import com.svalh.domain.source.DataBaseDataSource

class DBAddMovieRepositoryImpl(
    private val dataBaseDataSource: DataBaseDataSource
) : DBAddMovieRepository {

    override suspend fun addMoviesDB(domainMovie: DomainMovie) {
        return dataBaseDataSource.addMovieToCart(domainMovie)
    }
}