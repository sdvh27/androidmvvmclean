package com.svalh.data.repositories

import com.svalh.domain.common.Resource
import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.repositories.MovieDetailRepository
import com.svalh.domain.source.RemoteDataSource

class MovieDetailRepositoryImpl(
    private val remoteDataSource: RemoteDataSource
) : MovieDetailRepository {
    override suspend fun movieDetail(movieId: Int): Resource<DomainMovie> {
        return remoteDataSource.movieDetail(movieId)
    }
}