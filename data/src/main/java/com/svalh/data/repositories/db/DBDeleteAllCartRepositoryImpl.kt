package com.svalh.data.repositories.db

import com.svalh.domain.repositories.db.DBDeleteAllCartRepository
import com.svalh.domain.source.DataBaseDataSource

class DBDeleteAllCartRepositoryImpl(
    private val dataBaseDataSource: DataBaseDataSource
) : DBDeleteAllCartRepository {
    override suspend fun deleteAllCart() {
        dataBaseDataSource.deleteAllCart()
    }
}