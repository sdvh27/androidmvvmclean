package com.svalh.data.repositories

import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.repositories.PopularMovieRepository
import com.svalh.domain.source.DataBaseDataSource
import com.svalh.domain.source.RemoteDataSource

class PopularMoviesRepositoryImpl(
    private val dataBaseDataSource: DataBaseDataSource,
    private val remoteDataSource: RemoteDataSource
) : PopularMovieRepository {

    override suspend fun popularMovies(): List<DomainMovie> {
        if (dataBaseDataSource.isEmpty()) {
            val movies = remoteDataSource.listPopularMovies()
            movies.data?.let {
                dataBaseDataSource.saveMovies(it)
            }
        }

        return dataBaseDataSource.popularMovies()
    }
}