package com.svalh.data.repositories.db

import com.svalh.domain.repositories.db.DBDeleteMovieRepository
import com.svalh.domain.source.DataBaseDataSource

class DBDeleteMovieRepositoryImpl(
    private val dataBaseDataSource: DataBaseDataSource
) : DBDeleteMovieRepository {

    override suspend fun deleteMovieDB(id: Int) {
        dataBaseDataSource.deleteMovieFromCart(id)
    }
}