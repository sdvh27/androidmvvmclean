package com.svalh.data.repositories.db

import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.repositories.db.DBPopularMovieRepository
import com.svalh.domain.source.DataBaseDataSource


class DBPopularMoviesRepositoryImpl(
    private val dataBaseDataSource: DataBaseDataSource
) : DBPopularMovieRepository {

    override suspend fun popularMoviesDB(): List<DomainMovie> {
        return dataBaseDataSource.popularMovies()
    }
}