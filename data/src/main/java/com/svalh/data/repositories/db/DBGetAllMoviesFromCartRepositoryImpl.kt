package com.svalh.data.repositories.db

import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.repositories.db.DBGetAllMoviesFromCartRepository
import com.svalh.domain.source.DataBaseDataSource

class DBGetAllMoviesFromCartRepositoryImpl(
    private val dataBaseDataSource: DataBaseDataSource
) : DBGetAllMoviesFromCartRepository {
    override suspend fun getAllMoviesFromCart(): List<DomainMovie> {
        return dataBaseDataSource.getMoviesFromCart()
    }
}