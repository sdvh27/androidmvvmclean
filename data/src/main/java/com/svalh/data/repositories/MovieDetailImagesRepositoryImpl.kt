package com.svalh.data.repositories

import com.svalh.domain.common.Resource
import com.svalh.domain.entities.DomainMovieImages
import com.svalh.domain.repositories.MovieDetailImagesRepository
import com.svalh.domain.source.RemoteDataSource

class MovieDetailImagesRepositoryImpl(
    private val remoteDataSource: RemoteDataSource
) : MovieDetailImagesRepository {
    override suspend fun movieDetailImages(movieId: Int): Resource<DomainMovieImages> {
        return remoteDataSource.getMovieImage(movieId)
    }
}