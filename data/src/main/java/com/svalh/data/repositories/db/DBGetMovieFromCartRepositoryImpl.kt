package com.svalh.data.repositories.db

import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.repositories.db.DBGetMovieFromCartRepository
import com.svalh.domain.source.DataBaseDataSource

class DBGetMovieFromCartRepositoryImpl(
    private val dataBaseDataSource: DataBaseDataSource
) : DBGetMovieFromCartRepository {
    override suspend fun getMovieFromCart(id: Int): DomainMovie {
        return dataBaseDataSource.getMovieFromCartById(id)
    }
}