package com.svalh.data.database.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.svalh.data.database.entity.DataBaseAddGenre
import timber.log.Timber


class ConvertersCart {

    @TypeConverter
    fun toListGenres(json: String?): List<DataBaseAddGenre>? {
        return try {
            return json?.let {
                Gson().fromJson(it, Array<DataBaseAddGenre>::class.java).toList()
            }
        } catch (e: Exception) {
            Timber.e(e)
            null
        }
    }

    @TypeConverter
    fun toGenresData(genres: List<DataBaseAddGenre>): String? {
        return try {
            Gson().toJson(genres).toString()
        } catch (e: Exception) {
            Timber.e(e)
            null
        }
    }
}