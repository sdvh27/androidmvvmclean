package com.svalh.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.svalh.data.database.converters.Converters
import com.svalh.data.database.converters.ConvertersCart
import com.svalh.data.database.dao.MovieCartDao
import com.svalh.data.database.dao.MovieDAO
import com.svalh.data.database.entity.DataBaseAddMovie
import com.svalh.data.database.entity.DataBaseMovie
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

private const val DATA_BASE = "appDatabase.db"

@Database(entities = [DataBaseMovie::class, DataBaseAddMovie::class], version = 1)
@TypeConverters(Converters::class, ConvertersCart::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDAO
    abstract fun movieCartDao(): MovieCartDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context) : AppDatabase {
            return INSTANCE ?: synchronized(AppDatabase::class) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    DATA_BASE
                ).fallbackToDestructiveMigration().addCallback(AppDataBaseCallback()).build()
                INSTANCE = instance
                instance
            }
        }

        private class AppDataBaseCallback() : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                CoroutineScope(Dispatchers.IO).launch {
                    INSTANCE?.movieDao()?.deleteAll()
                }
            }
        }
    }
}