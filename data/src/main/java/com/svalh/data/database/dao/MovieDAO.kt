package com.svalh.data.database.dao

import androidx.room.*
import com.svalh.data.database.entity.DataBaseMovie

@Dao
interface MovieDAO {

    @Query("SELECT * FROM DataBaseMovie")
    fun getAll(): List<DataBaseMovie>

    @Query("SELECT * FROM DataBaseMovie WHERE id = :id")
    fun findById(id: Int): DataBaseMovie

    @Query("SELECT COUNT(id) FROM DataBaseMovie")
    fun movieCount(): Int

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertMovies(dataBaseMovies: List<DataBaseMovie>)

    @Update
    fun updateMovie(dataBaseMovie: DataBaseMovie)

    @Query("DELETE FROM DataBaseMovie")
    fun deleteAll()
}