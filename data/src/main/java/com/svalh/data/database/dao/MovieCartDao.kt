package com.svalh.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.svalh.data.database.entity.DataBaseAddMovie
import com.svalh.data.database.entity.DataBaseMovie

@Dao
interface MovieCartDao {
    @Query("SELECT * FROM DataBaseAddMovie")
    fun getAllFromCart(): List<DataBaseAddMovie>

    @Query("SELECT * FROM DataBaseMovie WHERE id = :id")
    fun getMovieFromCartById(id: Int): DataBaseMovie

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertMovieToCart(addMovie: DataBaseAddMovie)

    @Query("DELETE FROM DataBaseAddMovie WHERE id = :id")
    fun deleteMovieFromCart(id: Int)

    @Query("DELETE FROM DataBaseAddMovie")
    fun deleteAll()
}