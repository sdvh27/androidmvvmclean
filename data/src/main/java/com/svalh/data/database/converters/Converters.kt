package com.svalh.data.database.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.svalh.data.database.entity.DataBaseGenre
import timber.log.Timber

class Converters {

    @TypeConverter
    fun toListGenres(json: String?): List<DataBaseGenre>? {
        return try {
            return json?.let {
                Gson().fromJson(it, Array<DataBaseGenre>::class.java).toList()
            }
        } catch (e: Exception) {
            Timber.e(e)
            null
        }
    }

    @TypeConverter
    fun toGenresData(genres: List<DataBaseGenre>): String? {
        return try {
            Gson().toJson(genres).toString()
        } catch (e: Exception) {
            Timber.e(e)
            null
        }
    }
}