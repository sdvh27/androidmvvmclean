object Versions {
    val kotlin_version = "1.4.10"
    val tools_version = "4.0.2"
    val compile_sdk_version = 29
    val min_sdk_version = 21
    val target_sdk_version = 29
    val code_version = 1
    val name_version = "1.0"
    val core_ktx_version = "1.3.2"
    val appcompat_version = "1.2.0"
    val constraint_layout_version = "2.0.2"
    val retrofit_version = "2.8.1"
    val okhttp_version = "4.4.0"
    val room_version = "2.2.5"
    val timber_version = "1.5.1"
    val koin_version = "2.1.5"
    val coroutines_version = "1.3.3"
    val recyclerview_version = "1.1.0"
    val life_cycle_view_model_version = "2.2.0"
    val material_version = "1.1.0"
    val coil_version = "0.9.5"
    val stetho_version = "1.5.1"
    val glide_version = "4.11.0"
    val volley_version = "1.1.0"
    val nav_version = "2.3.1"
    val nav_safe_args_version = "2.3.1"
}

object Deps {
    val androidx_appcompat = "androidx.appcompat:appcompat:${Versions.appcompat_version}"
    val recyclerview = "androidx.recyclerview:recyclerview:${Versions.recyclerview_version}"
    val androidx_constraintlayout = "androidx.constraintlayout:constraintlayout:${Versions.constraint_layout_version}"
    val androidx_core =  "androidx.core:core-ktx:${Versions.core_ktx_version}"
    val lifeCycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.life_cycle_view_model_version}"
    val material = "com.google.android.material:material:${Versions.material_version}"
    val roomRuntime = "androidx.room:room-runtime:${Versions.room_version}"
    val roomKtx = "androidx.room:room-ktx:${Versions.room_version}"
    val roomCompiler = "androidx.room:room-compiler:${Versions.room_version}"
    val kotlin_stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin_version}"
    val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines_version}"
    val okhttpLoginInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp_version}"
    val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp_version}"
    val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit_version}"
    val retrofit_adapter = "com.squareup.retrofit2:adapter-rxjava:${Versions.retrofit_version}"
    val retrofit_converter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit_version}"
    val koin = "org.koin:koin-android:${Versions.koin_version}"
    val koinScope = "org.koin:koin-android-scope:${Versions.koin_version}"
    val koinViewModel = "org.koin:koin-android-viewmodel:${Versions.koin_version}"
    val timber = "com.github.ajalt:timberkt:${Versions.timber_version}"
    val coil = "io.coil-kt:coil:${Versions.coil_version}"
    val stetho_okhttp3 = "com.facebook.stetho:stetho-okhttp3:${Versions.stetho_version}"
    val stetho_url_conn = "com.facebook.stetho:stetho-urlconnection:${Versions.stetho_version}"
    val glide = "com.github.bumptech.glide:glide:${Versions.glide_version}"
    val volley = "com.android.volley:volley:${Versions.volley_version}"

    // Kotlin
    val navigation_fragment = "androidx.navigation:navigation-fragment-ktx:${Versions.nav_version}"
    val navigation_ui = "androidx.navigation:navigation-ui-ktx:${Versions.nav_version}"

    // Feature module Support
    val navigation_dynamic = "androidx.navigation:navigation-dynamic-features-fragment:${Versions.nav_version}"

    val navigation_safe_args = "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.nav_safe_args_version}"
    val kotlin_gradle_plugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin_version}"
    val kotlin_tools = "com.android.tools.build:gradle:${Versions.tools_version}"
}

object VersionsTests {
    val junit_version = "4.12"
    val junit_ext_version = "1.1.2"
    val espresso_version = "3.2.0"
    val espresso_core_version = "3.1.0"
    val mockito_kotlin_version = "2.2.0"
    val mockito_inline_version = "2.28.2"
    val koin_test_version = "2.1.1"
    val arch_core_version = "2.1.0"
    val test_runner_version = "1.2.0"
    val ext_junit_version = "1.1.1"
    val mock_web_server_version = "4.2.1"
    val okhttp_version = "1.0.0"
    val fragment_impl_version = "1.2.4"
    val fragment_ktx_version = "1.1.0"
    val nav_version = "2.3.1"
}

object Tests {
    val junit = "junit:junit:${VersionsTests.junit_version}"
    val androidx_junit = "androidx.test.ext:junit:${VersionsTests.junit_ext_version}"
    val espresso_core = "androidx.test.espresso:espresso-core:${VersionsTests.espresso_core_version}"
    val espresso = "androidx.test.espresso:espresso-contrib:${VersionsTests.espresso_version}"
    val mockitoKotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:${VersionsTests.mockito_kotlin_version}"
    val mockitoInline = "org.mockito:mockito-inline:${VersionsTests.mockito_inline_version}"
    val koinTest = "org.koin:koin-test:${VersionsTests.koin_test_version}"
    val archCoreTesting = "androidx.arch.core:core-testing:${VersionsTests.arch_core_version}"
    val testRunner = "androidx.test:runner:${VersionsTests.test_runner_version}"
    val rules = "androidx.test:rules:${VersionsTests.test_runner_version}"
    val extJunit = "androidx.test.ext:junit-ktx:${VersionsTests.ext_junit_version}"
    val mockWebServer = "com.squareup.okhttp3:mockwebserver:${VersionsTests.mock_web_server_version}"
    val okhttpIdling = "com.jakewharton.espresso:okhttp3-idling-resource:${VersionsTests.okhttp_version}"
    val fragmentImpl = "androidx.fragment:fragment-testing:${VersionsTests.fragment_impl_version}"
    val fragmentKtx = "androidx.fragment:fragment-ktx:${VersionsTests.fragment_ktx_version}"
    // Testing Navigation
    val navigation_test = "androidx.navigation:navigation-testing:${VersionsTests.nav_version}"
}