package com.svalh.domain.entities

data class DomainMovie(
    val backdropPath: String?,
    val domainGenres: ArrayList<DomainGenre>,
    val id: Int,
    val originalTitle: String,
    val overview: String,
    val posterPath: String?,
    val releaseDate: String,
    val title: String,
    val video: Boolean,
    val voteAverage: Double
)

data class DomainGenre(val id: Int, val name: String)