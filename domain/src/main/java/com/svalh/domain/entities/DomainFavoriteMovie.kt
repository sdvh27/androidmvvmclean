package com.svalh.domain.entities

data class DomainFavoriteMovie(
    val id: Int,
    val title: String?,
    val posterPath: String?
)