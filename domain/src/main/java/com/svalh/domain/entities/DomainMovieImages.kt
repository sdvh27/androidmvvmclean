package com.svalh.domain.entities

data class DomainMovieImages (
    val id: Int,
    val backdrops: List<DomainBackdrops>,
    val posters: List<DomainPosters>
)

data class DomainBackdrops(
    val aspectRatio: Double,
    val filePath: String,
    val height: Int,
    val iso_639_1: String?,
    val voteAverage: Double,
    val voteCount: Int,
    val width: Int
)

data class DomainPosters(
    val aspectRatio: Double,
    val filePath: String,
    val height: Int,
    val iso_639_1: String?,
    val voteAverage: Double,
    val voteCount: Int,
    val width: Int
)