package com.svalh.domain.repositories

import com.svalh.domain.common.Resource
import com.svalh.domain.entities.DomainMovieImages

interface MovieDetailImagesRepository {
    suspend fun movieDetailImages(movieId: Int): Resource<DomainMovieImages>
}