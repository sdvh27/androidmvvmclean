package com.svalh.domain.repositories.db

interface DBDeleteAllCartRepository {
    suspend fun deleteAllCart()
}