package com.svalh.domain.repositories.db

import com.svalh.domain.entities.DomainMovie

interface DBGetMovieFromCartRepository {
    suspend fun getMovieFromCart(id: Int) : DomainMovie
}