package com.svalh.domain.repositories.db

import com.svalh.domain.entities.DomainMovie

interface DBPopularMovieRepository {
    suspend fun popularMoviesDB(): List<DomainMovie>
}