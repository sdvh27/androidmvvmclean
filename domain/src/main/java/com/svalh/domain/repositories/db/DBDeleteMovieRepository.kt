package com.svalh.domain.repositories.db

interface DBDeleteMovieRepository {
    suspend fun deleteMovieDB(id: Int)
}