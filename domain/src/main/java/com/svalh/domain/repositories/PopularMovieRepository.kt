package com.svalh.domain.repositories

import com.svalh.domain.entities.DomainMovie

interface PopularMovieRepository {
    suspend fun popularMovies(): List<DomainMovie>
}