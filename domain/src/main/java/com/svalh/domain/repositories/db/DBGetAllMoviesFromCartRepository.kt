package com.svalh.domain.repositories.db

import com.svalh.domain.entities.DomainMovie

interface DBGetAllMoviesFromCartRepository {
    suspend fun getAllMoviesFromCart(): List<DomainMovie>
}