package com.svalh.domain.repositories

import com.svalh.domain.common.Resource
import com.svalh.domain.entities.DomainMovie

interface MovieDetailRepository {
    suspend fun movieDetail(movieId: Int): Resource<DomainMovie>
}