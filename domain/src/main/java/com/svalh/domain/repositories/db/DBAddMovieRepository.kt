package com.svalh.domain.repositories.db

import com.svalh.domain.entities.DomainMovie


interface DBAddMovieRepository {
    suspend fun addMoviesDB(domainMovie: DomainMovie)
}