package com.svalh.domain.source

import com.svalh.domain.entities.DomainMovie

interface DataBaseDataSource {
    suspend fun popularMovies(): List<DomainMovie>
    suspend fun saveMovies(movies: List<DomainMovie>)
    suspend fun isEmpty(): Boolean
    suspend fun addMovieToCart(movies: DomainMovie)
    suspend fun getMovieFromCartById(id: Int): DomainMovie
    suspend fun deleteMovieFromCart(id: Int)
    suspend fun getMoviesFromCart() : List<DomainMovie>
    suspend fun deleteAllCart()
}