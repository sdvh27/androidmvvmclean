package com.svalh.domain.source

import com.svalh.domain.common.Resource
import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.entities.DomainMovieImages

interface RemoteDataSource {
    suspend fun listPopularMovies(): Resource<List<DomainMovie>>
    suspend fun getMovieImage(id: Int) : Resource<DomainMovieImages>
    suspend fun movieDetail(movieId: Int): Resource<DomainMovie>
}