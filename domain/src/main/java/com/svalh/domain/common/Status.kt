package com.svalh.domain.common

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
