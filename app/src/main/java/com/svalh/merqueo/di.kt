package com.svalh.merqueo

import android.app.Application
import com.svalh.data.database.AppDatabase
import com.svalh.data.repositories.MovieDetailImagesRepositoryImpl
import com.svalh.data.repositories.MovieDetailRepositoryImpl
import com.svalh.data.repositories.PopularMoviesRepositoryImpl
import com.svalh.data.repositories.db.*
import com.svalh.data.service.api.ApiClient
import com.svalh.data.service.source.RoomDataSource
import com.svalh.domain.repositories.MovieDetailImagesRepository
import com.svalh.domain.repositories.MovieDetailRepository
import com.svalh.domain.repositories.PopularMovieRepository
import com.svalh.domain.repositories.db.*
import com.svalh.domain.source.DataBaseDataSource
import com.svalh.interactors.GetMovieDetail
import com.svalh.interactors.GetMovieDetailImages
import com.svalh.interactors.GetPopularMovies
import com.svalh.interactors.db.*
import com.svalh.merqueo.ui.cartFragment.CartFragment
import com.svalh.merqueo.ui.cartFragment.CartViewModel
import com.svalh.merqueo.ui.main.MainActivity
import com.svalh.merqueo.ui.common.NetworkHelper
import com.svalh.merqueo.ui.detailFragment.DetailFragment
import com.svalh.merqueo.ui.detailFragment.DetailViewModel
import com.svalh.merqueo.ui.homeFragment.HomeFragment
import com.svalh.merqueo.ui.homeFragment.HomeViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.qualifier.named
import org.koin.dsl.module

fun Application.initDI() {
    startKoin {
        androidLogger()
        androidContext(this@initDI)
        androidFileProperties()
        koin.loadModules(listOf(appModule, dataModule, scopesModule))
        koin.createRootScope()
    }
}

private val appModule = module {
    single { NetworkHelper(get()) }
    single(named(apiKey)) { apiKeyValue }
    single { ApiClient(get()) }
    single { ApiClient(get()).moviDbServices }
    single { ApiClient(get()).volleyRequest }
    single { AppDatabase.getInstance(get()) }
    single<CoroutineDispatcher> { Dispatchers.Main }
    single(named(ioScope)) { Dispatchers.IO }
    factory<DataBaseDataSource> { RoomDataSource(get()) }
    factory<com.svalh.domain.source.RemoteDataSource> { com.svalh.data.service.source.RemoteDataSource(get(), get(named(apiKey)), get()) }
}

val dataModule = module {
    factory<PopularMovieRepository> { PopularMoviesRepositoryImpl(get(), get()) }
    factory<DBPopularMovieRepository> { DBPopularMoviesRepositoryImpl(get()) }
    factory<DBAddMovieRepository> { DBAddMovieRepositoryImpl(get()) }
    factory<DBDeleteMovieRepository> { DBDeleteMovieRepositoryImpl(get()) }
    factory<MovieDetailRepository> { MovieDetailRepositoryImpl(get()) }
    factory<MovieDetailImagesRepository> { MovieDetailImagesRepositoryImpl(get()) }
    factory<DBGetMovieFromCartRepository> { DBGetMovieFromCartRepositoryImpl(get()) }
    factory<DBGetAllMoviesFromCartRepository> { DBGetAllMoviesFromCartRepositoryImpl(get()) }
    factory<DBDeleteAllCartRepository> { DBDeleteAllCartRepositoryImpl(get()) }
}

private val scopesModule = module {
    scope(named<MainActivity>()) {

    }

    scope(named<HomeFragment>()) {
        viewModel { HomeViewModel(get(), get(), get(), get(), get(), get(),get(named(ioScope))) }
        scoped { GetPopularMovies(get()) }
        scoped { DBGetPopularMovies(get()) }
        scoped { DBAddMovieToCart(get()) }
        scoped { DBDeleteMovieFromCart(get()) }
    }

    scope(named<DetailFragment>()) {
        viewModel { DetailViewModel(get(), get(), get(), get(), get(), get(), get()) }
        scoped { GetMovieDetail(get()) }
        scoped { GetMovieDetailImages(get()) }
        scoped { DBGetMovieFromCart(get()) }
        scoped { DBAddMovieToCart(get()) }
        scoped { DBDeleteMovieFromCart(get()) }
    }

    scope(named<CartFragment>()) {
        viewModel { CartViewModel(get(), get(), get()) }
        scoped { DBGetAllMoviesFromCart(get()) }
        scoped { DBDeleteAllCart(get()) }
    }
}

private const val apiKeyValue = "4409d04687217e7763238e91871d61c3"
private const val apiKey = "apiKey"
private const val ioScope = "io_scope"