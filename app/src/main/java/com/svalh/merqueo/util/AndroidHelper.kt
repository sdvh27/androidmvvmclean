package com.svalh.merqueo.util

import android.content.Context
import com.svalh.data.service.api.Constants

object AndroidHelper {
    private var context: Context? = null

    fun init(context: Context) {
        this.context = context
    }

    fun getString(id: Int): String {
        return context?.getString(id) ?: Constants.DEFAULT_STRING
    }
}