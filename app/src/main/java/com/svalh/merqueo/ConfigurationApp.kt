package com.svalh.merqueo

import android.app.Application
import com.facebook.stetho.Stetho
import com.svalh.merqueo.util.AndroidHelper

class ConfigurationApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        AndroidHelper.init(applicationContext)
        initDI()
    }
}