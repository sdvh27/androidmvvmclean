package com.svalh.merqueo.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.svalh.domain.entities.DomainMovie
import com.svalh.merqueo.R
import com.svalh.merqueo.ui.common.basicDiffUtil
import com.svalh.merqueo.ui.common.inflate
import com.svalh.merqueo.ui.common.loadUrl
import kotlinx.android.synthetic.main.view_movie.view.*

class MoviesAdapter(
        private val listenerNav: (DomainMovie) -> Unit,
        private val listenerAdd: (DomainMovie) -> Unit,
        private val listenerDelete: (DomainMovie) -> Unit
) :
        RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    var movies: List<DomainMovie> by basicDiffUtil(
            emptyList(),
            areItemsTheSame = { old, new -> old.id == new.id }
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.inflate(R.layout.view_movie, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = movies.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = movies[position]
        holder.bind(movie)
        holder.itemView.movieCover.setOnClickListener { listenerNav(movie) }
        holder.itemView.movieAdd.setOnClickListener { listenerAdd(movie) }
        holder.itemView.movieDelete.setOnClickListener { listenerDelete(movie) }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(movie: DomainMovie) {
            itemView.movieTitle.text = movie.title
            itemView.movieCover.loadUrl("$BASE_URL${movie.posterPath}")
        }
    }

    companion object {
        private const val BASE_URL = "https://image.tmdb.org/t/p/w185/"
    }
}