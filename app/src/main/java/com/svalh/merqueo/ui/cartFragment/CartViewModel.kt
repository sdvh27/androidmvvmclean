package com.svalh.merqueo.ui.cartFragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svalh.interactors.db.DBDeleteAllCart
import com.svalh.interactors.db.DBGetAllMoviesFromCart
import com.svalh.merqueo.scopes.ScopeViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch

class CartViewModel(
    private val dbGetAllMoviesFromCart: DBGetAllMoviesFromCart,
    private val dbDeleteAllCart: DBDeleteAllCart,
    uiDispatcher: CoroutineDispatcher
) : ScopeViewModel(uiDispatcher) {
    // TODO: Implement the ViewModel
    init {
        createScope()
    }

    private val _model = MutableLiveData<CartUiModel>()
    val model: LiveData<CartUiModel>
        get() {
            if (_model.value == null) {
                loadMovies()
            }
            return _model
        }

    private fun loadMovies() {
        launch {
            _model.value = CartUiModel.Loading
            _model.value = CartUiModel.Content(dbGetAllMoviesFromCart.invoke())
        }
    }

    fun deleteAllCart() = launch {
        dbDeleteAllCart.invoke()
    }

    override fun onCleared() {
        destroyScope()
        super.onCleared()
    }
}