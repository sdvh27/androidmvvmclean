package com.svalh.merqueo.ui.cartFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.svalh.merqueo.R
import com.svalh.merqueo.adapters.CartAdapter
import kotlinx.android.synthetic.main.cart_fragment.*
import org.koin.android.scope.lifecycleScope
import org.koin.android.viewmodel.scope.viewModel

class CartFragment : Fragment() {

    companion object {
        fun newInstance() = CartFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // This callback will only be called when MyFragment is at least Started.

        // This callback will only be called when MyFragment is at least Started.
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    // Handle the back button event
                    findNavController().navigateUp()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    private val viewModel: CartViewModel by lifecycleScope.viewModel(this)
    private lateinit var adapter: CartAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.cart_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter = CartAdapter()
        recycler.adapter = adapter

        // TODO: Use the ViewModel
        viewModel.model.observe(viewLifecycleOwner, Observer(::updateUi))

        btn_delete_cart.setOnClickListener {
            viewModel.deleteAllCart()
            val action = CartFragmentDirections.actionCartFragmentToMainFragment()
            findNavController().navigate(action)
        }
    }

    private fun updateUi(model: CartUiModel) {
        progress.visibility = if (model == CartUiModel.Loading) View.VISIBLE else View.GONE

        when (model) {
            is CartUiModel.Content -> adapter.movies = model.movies
        }
    }
}