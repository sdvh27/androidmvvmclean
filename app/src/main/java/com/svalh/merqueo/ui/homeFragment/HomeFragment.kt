package com.svalh.merqueo.ui.homeFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.svalh.merqueo.R
import com.svalh.merqueo.adapters.MoviesAdapter
import com.svalh.merqueo.ui.common.showMessage
import kotlinx.android.synthetic.main.home_fragment.*
import org.koin.android.scope.lifecycleScope
import org.koin.android.viewmodel.scope.viewModel

class HomeFragment : Fragment() {

    private val viewModel: HomeViewModel by lifecycleScope.viewModel(this)
    private lateinit var adapter: MoviesAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
        adapter = MoviesAdapter(viewModel::onMovieClickedNav, viewModel::onMovieClickedAdd, viewModel::onMovieClickedDelete)
        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)

        viewModel.model.observe(viewLifecycleOwner, Observer(::updateUi))
        viewModel.navigation.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { result ->
                val action = HomeFragmentDirections.actionMainFragmentToDestinationFragment(result.id)
                findNavController().navigate(action)
            }
        })
        viewModel.addMovie.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { result ->
                viewModel.addMoviesToCart(result)
                activity?.showMessage("Pelicula añadida al carro")
            }
        })
        viewModel.deleteMovie.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { result ->
                viewModel.deleteMoviesToCart(result.id)
                activity?.showMessage("Pelicula eliminada del carro")
            }
        })

        btn_cart.setOnClickListener {
            val action = HomeFragmentDirections.actionMainFragmentToCartFragment()
            findNavController().navigate(action)
        }
    }

    private fun updateUi(model: HomeUiModel) {
        progress.visibility = if (model == HomeUiModel.Loading) View.VISIBLE else View.GONE

        when (model) {
            is HomeUiModel.Content -> adapter.movies = model.movies
        }
    }
}