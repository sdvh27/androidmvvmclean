package com.svalh.merqueo.ui.detailFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.svalh.domain.entities.DomainMovie
import com.svalh.merqueo.R
import com.svalh.merqueo.ui.common.loadUrl
import com.svalh.merqueo.ui.common.showMessage
import kotlinx.android.synthetic.main.detail_fragment.*
import org.koin.android.scope.lifecycleScope
import org.koin.android.viewmodel.scope.viewModel


class DetailFragment : Fragment() {

    private val viewModel: DetailViewModel by lifecycleScope.viewModel(this)
    private var movieId: Int? = null
    private lateinit var movie: DomainMovie

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // This callback will only be called when MyFragment is at least Started.

        // This callback will only be called when MyFragment is at least Started.
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    // Handle the back button event
                    findNavController().navigateUp()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
        val args: DetailFragmentArgs by navArgs()
        args.let {
            movieId = it.id
        }
        movieId.let {
            viewModel.loadMovie(it!!)
            viewModel.modelMovie.observe(viewLifecycleOwner, Observer { model ->
                movie = model
                movieTitle.text = model.title
                movieDescription.text = model.overview
                viewModel.loadMovieImage(it)
                viewModel.modelMovieImage.observe(viewLifecycleOwner, Observer { modelMovieImages ->
                    if (modelMovieImages != null) {
                        movieCover.loadUrl("https://image.tmdb.org/t/p/w185/${modelMovieImages.posters[0].filePath}")
                    } else {
                        movieCover.loadUrl("https://image.tmdb.org/t/p/w185/${model.posterPath}")
                    }
                })
            })
        }

        movieAdd.setOnClickListener {
            viewModel.addMoviesToCart(movie)
            activity?.showMessage("Pelicula añadida al carro")
        }
        movieDelete.setOnClickListener {
            viewModel.deleteMoviesToCart(movieId!!)
            activity?.showMessage("Pelicula eliminada del carro")
        }
    }
}