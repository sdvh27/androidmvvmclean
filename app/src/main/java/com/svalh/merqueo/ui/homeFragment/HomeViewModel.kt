package com.svalh.merqueo.ui.homeFragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svalh.domain.entities.DomainMovie
import com.svalh.interactors.GetPopularMovies
import com.svalh.interactors.db.DBAddMovieToCart
import com.svalh.interactors.db.DBDeleteMovieFromCart
import com.svalh.interactors.db.DBGetPopularMovies
import com.svalh.merqueo.scopes.ScopeViewModel
import com.svalh.merqueo.ui.Event
import com.svalh.merqueo.ui.common.NetworkHelper
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import androidx.lifecycle.viewModelScope

class HomeViewModel(
        private val getPopularMovies: GetPopularMovies,
        private val dbGetPopularMovies: DBGetPopularMovies,
        private val dbAddMovieToCart: DBAddMovieToCart,
        private val dbDeleteMovieFromCart: DBDeleteMovieFromCart,
        private val networkHelper: NetworkHelper,
        uiDispatcher: CoroutineDispatcher,
        private val ioScope: CoroutineDispatcher
) : ScopeViewModel(uiDispatcher) {

    init {
        createScope()
    }

    private val _model = MutableLiveData<HomeUiModel>()
    val model: LiveData<HomeUiModel>
        get() {
            if (_model.value == null) {
                loadMovies()
            }
            return _model
        }

    private val _navigation = MutableLiveData<Event<DomainMovie>>()
    val navigation: LiveData<Event<DomainMovie>> = _navigation

    private val _addMovie = MutableLiveData<Event<DomainMovie>>()
    val addMovie: LiveData<Event<DomainMovie>> = _addMovie

    private val _deleteMovie = MutableLiveData<Event<DomainMovie>>()
    val deleteMovie: LiveData<Event<DomainMovie>> = _deleteMovie

    private fun loadMovies() {
        viewModelScope.launch {
            _model.value = HomeUiModel.Loading
            if (networkHelper.isNetworkConnected()) {
                _model.value = HomeUiModel.Content(getPopularMovies.invoke())
            } else {
                _model.value = HomeUiModel.Content(dbGetPopularMovies.invoke())
            }
        }
    }

    fun addMoviesToCart(domainMovie: DomainMovie) = viewModelScope.launch(ioScope) {
        dbAddMovieToCart.invoke(domainMovie)
    }

    fun deleteMoviesToCart(id: Int) = viewModelScope.launch(ioScope) {
        dbDeleteMovieFromCart.invoke(id)
    }

    fun onMovieClickedNav(movie: DomainMovie) {
        _navigation.value = Event(movie)
    }

    fun onMovieClickedAdd(movie: DomainMovie) {
        _addMovie.value = Event(movie)
    }

    fun onMovieClickedDelete(movie: DomainMovie) {
        _deleteMovie.value = Event(movie)
    }

    override fun onCleared() {
        destroyScope()
        super.onCleared()
    }
}