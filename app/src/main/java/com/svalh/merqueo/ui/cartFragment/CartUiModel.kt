package com.svalh.merqueo.ui.cartFragment

import com.svalh.domain.entities.DomainMovie

sealed class CartUiModel {
    object Loading : CartUiModel()
    data class Content(val movies: List<DomainMovie>) : CartUiModel()
}