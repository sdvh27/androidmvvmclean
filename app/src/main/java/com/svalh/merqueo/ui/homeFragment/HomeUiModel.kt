package com.svalh.merqueo.ui.homeFragment

import com.svalh.domain.entities.DomainMovie

sealed class HomeUiModel {
    object Loading : HomeUiModel()
    data class Content(val movies: List<DomainMovie>) : HomeUiModel()
}