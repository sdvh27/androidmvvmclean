package com.svalh.merqueo.ui.detailFragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.entities.DomainMovieImages
import com.svalh.interactors.GetMovieDetail
import com.svalh.interactors.GetMovieDetailImages
import com.svalh.interactors.db.DBAddMovieToCart
import com.svalh.interactors.db.DBDeleteMovieFromCart
import com.svalh.interactors.db.DBGetMovieFromCart
import com.svalh.merqueo.scopes.ScopeViewModel
import com.svalh.merqueo.ui.common.NetworkHelper
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch

class DetailViewModel(
    private val getMovieDetail: GetMovieDetail,
    private val getMovieDetailImages: GetMovieDetailImages,
    private val dbGetMovieFromCart: DBGetMovieFromCart,
    private val dbAddMovieToCart: DBAddMovieToCart,
    private val dbDeleteMovieFromCart: DBDeleteMovieFromCart,
    private val networkHelper: NetworkHelper,
    uiDispatcher: CoroutineDispatcher
) : ScopeViewModel(uiDispatcher) {

    init {
        createScope()
    }

    private val _modelMovie = MutableLiveData<DomainMovie>()
    val modelMovie: LiveData<DomainMovie> = _modelMovie

    private val _modelMovieImage = MutableLiveData<DomainMovieImages>()
    val modelMovieImage: LiveData<DomainMovieImages> = _modelMovieImage

    fun loadMovie(movieId: Int) {
        launch {
            if (networkHelper.isNetworkConnected()) {
                val movie = getMovieDetail.invoke(movieId)
                _modelMovie.value = movie.data
            } else {
                _modelMovie.value = dbGetMovieFromCart.invoke(movieId)
            }
        }
    }

    fun loadMovieImage(movieId: Int) {
        launch {
            if (networkHelper.isNetworkConnected()) {
                val movie = getMovieDetailImages.invoke(movieId)
                _modelMovieImage.value = movie.data
            } else {
                _modelMovieImage.value = null
            }
        }
    }

    fun addMoviesToCart(domainMovie: DomainMovie) = launch {
        dbAddMovieToCart.invoke(domainMovie)
    }

    fun deleteMoviesToCart(id: Int) = launch {
        dbDeleteMovieFromCart.invoke(id)
    }

    override fun onCleared() {
        destroyScope()
        super.onCleared()
    }
}