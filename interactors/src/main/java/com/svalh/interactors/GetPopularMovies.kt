package com.svalh.interactors

import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.repositories.PopularMovieRepository

class GetPopularMovies(
    private val movieRepository: PopularMovieRepository
) {
    suspend fun invoke(): List<DomainMovie> = movieRepository.popularMovies()
}