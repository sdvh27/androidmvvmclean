package com.svalh.interactors.db

import com.svalh.domain.repositories.db.DBDeleteMovieRepository

class DBDeleteMovieFromCart(
    private val dbDeleteMovieRepository: DBDeleteMovieRepository
) {

    suspend fun invoke(id: Int) = dbDeleteMovieRepository.deleteMovieDB(id)
}