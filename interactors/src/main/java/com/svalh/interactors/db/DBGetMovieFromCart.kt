package com.svalh.interactors.db

import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.repositories.db.DBGetMovieFromCartRepository

class DBGetMovieFromCart(
    private val dbGetMovieFromCartRepository: DBGetMovieFromCartRepository
) {
    suspend fun invoke(movieId: Int) : DomainMovie = dbGetMovieFromCartRepository.getMovieFromCart(movieId)
}