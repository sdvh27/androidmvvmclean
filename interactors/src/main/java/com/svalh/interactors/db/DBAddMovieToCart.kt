package com.svalh.interactors.db

import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.repositories.db.DBAddMovieRepository

class DBAddMovieToCart(
    private val dbAddMovieRepository: DBAddMovieRepository
) {
    suspend fun invoke(domainMovie: DomainMovie) = dbAddMovieRepository.addMoviesDB(domainMovie)
}