package com.svalh.interactors.db

import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.repositories.db.DBPopularMovieRepository

class DBGetPopularMovies(
    private val dbPopularMovieRepository: DBPopularMovieRepository
) {
    suspend fun invoke() : List<DomainMovie> = dbPopularMovieRepository.popularMoviesDB()
}