package com.svalh.interactors.db

import com.svalh.domain.repositories.db.DBDeleteAllCartRepository

class DBDeleteAllCart(
    private val dbDeleteAllCartRepository: DBDeleteAllCartRepository
) {
    suspend fun invoke() = dbDeleteAllCartRepository.deleteAllCart()
}