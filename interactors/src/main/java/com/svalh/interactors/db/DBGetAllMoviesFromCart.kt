package com.svalh.interactors.db

import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.repositories.db.DBGetAllMoviesFromCartRepository

class DBGetAllMoviesFromCart(
    private val dbGetAllMoviesFromCartRepository: DBGetAllMoviesFromCartRepository
) {
    suspend fun invoke() : List<DomainMovie> = dbGetAllMoviesFromCartRepository.getAllMoviesFromCart()
}