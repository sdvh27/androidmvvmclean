package com.svalh.interactors

import com.svalh.domain.common.Resource
import com.svalh.domain.entities.DomainMovieImages
import com.svalh.domain.repositories.MovieDetailImagesRepository

class GetMovieDetailImages(
    private val movieDetailImagesRepository: MovieDetailImagesRepository
) {
    suspend fun invoke(movieId: Int) : Resource<DomainMovieImages> = movieDetailImagesRepository.movieDetailImages(movieId)
}