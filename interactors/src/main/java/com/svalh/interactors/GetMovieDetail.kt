package com.svalh.interactors

import com.svalh.domain.common.Resource
import com.svalh.domain.entities.DomainMovie
import com.svalh.domain.repositories.MovieDetailRepository

class GetMovieDetail(
    private val movieDetailRepository: MovieDetailRepository
) {
    suspend fun invoke(id: Int) : Resource<DomainMovie> = movieDetailRepository.movieDetail(id)
}