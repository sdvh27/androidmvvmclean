node('cpu-intensive-docker-builds-slave') {
    def slackChannel = '#myOwnSlackMerqueoChannel'

    library identifier: "${env.DEFAULT_SHARED_LIBS}",
    retriever: modernSCM([$class: 'GitSCMSource',
        remote: "${env.DEFAULT_SHARED_LIBS_REPO}"])

    pipelineProps.defaultBuildMultibranchProperties()

    try {
       stage('Checkout') {
                   logger.stage()
                   timeout(5) {
                       checkout scm

                       withVault(vaultSecrets: [
                            [path: "jenkins/merqueo/services/map_licenses", secretValues: [
                                    [envVar: 'licenses_properties', vaultKey: 'licenses_properties'],
                                    [envVar: 'portal_properties', vaultKey: 'portal_properties']
                                ]
                            ]
                       ]) {
                           sh """
                           rm -f $WORKSPACE/licenses.properties
                           cat > $WORKSPACE/licenses.properties << EOL\n$licenses_properties\nEOL
                           rm -f $WORKSPACE/portal.properties
                           cat > $WORKSPACE/portal.properties << EOL\n$portal_properties\nEOL
                           """
                       }
                   }
        }

        slackUtils.notifyBuild('STARTED', slackChannel)

        stage('Login to AWS') {
            timeout(5) {
                awsUtils.loginToAWS()
            }
        }
        docker.image("MyOwnMerqueoAmazonAwsServiceSdk").inside {
            stage('Clean builds'){
                timeout(5){
                    sh "./gradlew clean"
                }
            }
            stage('Build project'){
                timeout(30) {
                    sh "./gradlew buildDebug --stacktrace"
                }
            }
            stage('KLint Check'){
                timeout(5) {
                    sh "./gradlew ktlintcheck"
                }
            }
            stage('Unit Tests') {
                timeout(10) {
                    sh "./gradlew testDebugUnitTestCoverage --stacktrace"
                }
            }
            stage('Sonar') {
                timeout(5) {
                    sh "./gradlew sonarqube"
                }
            }

            if(env.BRANCH_NAME == 'develop' || env.BRANCH_NAME.contains('RELEASE')) {
                stage('Generate APK'){
                    timeout(10){
                        sh "./gradlew assemble --stacktrace"
                    }
                }

                stage('Archive APK'){
                    timeout(10){
                        def artifactPath = "app/build/outputs/apk"
                        dir("app/build/outputs/apk") {
                            sh "mv debug/app-debug.apk debug/app-debug-${BUILD_NUMBER}.apk"
                            sh "mv qatest/app-qatest.apk qatest/app-qatest-${BUILD_NUMBER}.apk"
                            sh "mv staging/app-staging.apk staging/app-staging-${BUILD_NUMBER}.apk"
                            sh "mv release/app-release.apk release/app-release-${BUILD_NUMBER}.apk"
                            archiveArtifacts "debug/app-debug-${BUILD_NUMBER}.apk,qatest/app-qatest-${BUILD_NUMBER}.apk,staging/app-staging-${BUILD_NUMBER}.apk,release/app-release-${BUILD_NUMBER}.apk"
                        }
                    }
                }

                slackUtils.notifyBuild("New application has been upload", slackChannel)
            }
        }
    } catch(ex) {
        currentBuild.result = 'FAILURE'
        throw ex
    } finally {
        stage('Notify') {
                slackUtils.notifyBuild(currentBuild.result, slackChannel)
        }

        cleanWs()
    }
}
