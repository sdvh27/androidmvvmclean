- Requermientos tecnicos para construir el proyecto:

	Android Studio con la ultima version 

- Descripcion ternica para el funcionamiento del deepLink:

	Una vez en el pantalla home dar click en cada pelicula para ver su informacion, agegarla o eliminarla

- Breve descripcion de la capa propuesta:

	La capa app: es la capa mas alta donde se encuentras las vistas y fragmentos asociados, en esta capa tenemos los liveData 
	encargados de notificar a cualquier vista cuando la informacion cambia, desde esta capa vamos a injectar todos los objetos 
	necesarios en cualquier sitio de la aplicacion que lo requiera, alli mismo esta construido el archivo que se encarga
	de inyectar dependencias.

	El folder BuildSrc: es donde encontramos referenciadas todas nuestras dependencias

	En la capa domain: encontraremos los objetos dedicados a la capa del negocio que necesita la aplicacion, adicional encontraremos
	algunos recursos locales y remotos que nos permite dar funcionamiento a la aplicacion, en esta capa se divide por una parte
	el repositorio de la aplicacion.

	En la capa data: al ser la capa mas baja de la app podremos consultar informacion como lo deseemos tanto del aspecto local 
	como del aspecto remoto, alli se hacen peticiones tanto a los servidores como a la base de datos de la aplicacion

	En la capa interactors: encontraremos flujos necesarios como casos de usos que nos permiten organizar de manera mas limpia
	como fluye la informacion en la aplicacion

- Fotos de la aplicacion:

	Las fotos pueden ser encontradas en la raiz del proyecto llamadas : detailscreen, homescreen, infoscreen



		